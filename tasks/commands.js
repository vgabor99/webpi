/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

var config = require('./.taskconfig');
var gulp = require('gulp');
var path = require('path');
var spawn = require('child_process').spawn;
var rsync = require('gulp-rsync');

/**
 * Runs Django shell.
 */
gulp.task('shell', function () {
  spawn('python', [path.join(config.paths.src, 'manage.py'), 'shell'], {
    stdio: 'inherit'
  });
});

/**
 * Runs Django migration.
 */
gulp.task('migrate', function () {
  spawn('python', [path.join(config.paths.src, 'manage.py'), 'migrate'], {
    stdio: 'inherit'
  });
});

/**
 * Deploys to production server.
 */
gulp.task('deploy', function () {
  return gulp.src(config.deploy.entry)
    .pipe(rsync({
      root: config.deploy.root,
      hostname: config.deploy.hostname,
      destination: config.deploy.destination,
      username: config.deploy.username,
      recursive: true
    }))
    .on('error', function (err) {
      console.log(err);
    });
});

