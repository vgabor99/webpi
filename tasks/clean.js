/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

var config = require('./.taskconfig');
var del = require('del');
var gulp = require('gulp');

/**
 * Cleans /.tmp and /build directories.
 */
gulp.task('clean', function(callback) {
  del(config.clean.entry).then(function(paths) {
    callback();
  });
});
