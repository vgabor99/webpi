#!/usr/bin/env python

"""
webpi
(c) Gabor Varga <gabor.varga.99@gmail.com>

This software is released under the MIT License:
http://www.opensource.org/licenses/mit-license.php
"""

import os
import sys

if __name__ == "__main__":
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings.dev")

  from django.core.management import execute_from_command_line

  execute_from_command_line(sys.argv)
