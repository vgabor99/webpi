from datetime import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

GUESTBOOK_ENTRY_MAX_LENGTH = getattr(settings, 'GUESTBOOK_ENTRY_MAX_LENGTH', 3000)


class Entry(models.Model):
    class Meta:
        ordering = ['submit_date', 'name']

    submit_date = models.DateTimeField(_('date'), default=datetime.now(), blank=True)
    name = models.CharField(_('name'), max_length=200)
    text = models.TextField(_('comment'), max_length=GUESTBOOK_ENTRY_MAX_LENGTH)

    def __unicode__(self):
        return _("%(name)s on %(date)s") % {'name': self.name,
                                            'date': self.submit_date}

    def save(self, force_insert=False, force_update=False):
        if self.submit_date is None:
            self.submit_date = datetime.datetime.now()
        super(Entry, self).save(force_insert, force_update)
