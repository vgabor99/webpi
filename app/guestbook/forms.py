# -*- coding: utf-8 -*-

import datetime
import time

from django import forms
from django.conf import settings

from models import Entry

GUESTBOOK_ENTRY_MAX_LENGTH = getattr(settings, 'GUESTBOOK_ENTRY_MAX_LENGTH', 3000)


class EntryForm(forms.Form):
    name = forms.CharField(label="Név", max_length=50, required=False)
    text = forms.CharField(label='Bejegyzés', widget=forms.Textarea,
                           max_length=GUESTBOOK_ENTRY_MAX_LENGTH, error_messages={'required': "Írj a bejegyzésbe"})
    timestamp = forms.IntegerField(widget=forms.HiddenInput, initial=int(time.time()))

    def get_entry_object(self):
        """
        Return a new (unsaved) comment object based on the information in this
        form. Assumes that the form is already validated and will throw a
        ValueError if not.

        Does not set any of the fields that would come from a Request object
        (i.e. ``user`` or ``ip_address``).
        """
        if not self.is_valid():
            raise ValueError("get_entry_object may only be called on valid forms")

        new = Entry(
            name=self.cleaned_data["name"],
            text=self.cleaned_data["text"],
            submit_date=datetime.datetime.now(),
        )

        # Check that this comment isn't duplicate. (Sometimes people post comments
        # twice by mistake.) If it is, fail silently by returning the old comment.
        possible_duplicates = Entry.objects.filter(
            name=new.name
        )
        for old in possible_duplicates:
            if old.submit_date.date() == new.submit_date.date() and old.text == new.text:
                return old

        return new

    def clean_timestamp(self):
        """Make sure the timestamp isn't too far (> 2 hours) in the past."""
        ts = self.cleaned_data["timestamp"]
        if time.time() - ts > (2 * 60 * 60):
            raise forms.ValidationError("Timestamp too old")
        return ts
