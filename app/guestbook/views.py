from django import http
from django.core.urlresolvers import reverse
from django.views.generic import ListView, FormView
from django.views.generic.edit import FormMixin

from forms import EntryForm
from models import Entry


class EntryList(FormMixin, ListView):
    '''
    Paginated guestbook entry list.
    '''
    model = Entry
    paginate_by = 5
    paginate_orphans = 2
    form_class = EntryForm

    def get_context_data(self, **kwargs):
        context = super(EntryList, self).get_context_data(**kwargs)
        context['form'] = EntryForm()
        return context


class EntryPost(FormView):
    '''
    Post guestbook entry. If the form has errors, or it was submitted for preview,
    display the form in the preview page. Otherwise save the form and redirect to the list view.
    '''
    template_name = 'guestbook/preview.html'
    form_class = EntryForm

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        data = self.request.POST.copy()
        preview = data.get("submit-preview", None) is not None
        if (preview or form.errors):
            return self.render_to_response(self.get_context_data(form=form))
        else:
            form.get_entry_object().save()
            return http.HttpResponseRedirect(reverse('guestbook-page-last'))
