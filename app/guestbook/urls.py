from django.conf.urls import patterns, url

from views import EntryList, EntryPost

urlpatterns = patterns('',
                       url(r'^$', EntryList.as_view(), name='guestbook-root'),
                       url(r'^\?page=last$', EntryList.as_view(), name='guestbook-page-last'),
                       url(r'^(?P<page>[0-9]+)/$', EntryList.as_view(), name='guestbook-page'),
                       url(r'^post/', EntryPost.as_view(), name='guestbook-post'),
                       )
