(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();

    // On mobile browsers, if you change scrolling direction, the URL bar gets hidden/revealed,
    // this changes the viewport size and causes flicker in % and vh dimensions. Looks very bad
    // for the parallax images. So we take over resizing - if the height change is small, we
    // ignore it. Larger changes e.g screen orientation changes) must be kept though.
    var HEIGHT_CHANGE_TOLERANCE = 100; // Approximately URL bar height in Chrome on tablet
    var viewportHeight = $(window).height();
    $(window).resize(function () {
      if (Math.abs(viewportHeight - $(window).height()) > HEIGHT_CHANGE_TOLERANCE) {
        viewportHeight = $(window).height();
        update();
      }
    });
    function update() {
      $('.parallax-container').css('height', viewportHeight*0.4 + 'px');
    }
    update();
  }); // end of document ready
})(jQuery); // end of jQuery name space
