/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

'use strict';

var rebound = require('rebound');

function rad(deg) {
  return deg * Math.PI / 180;
}

function Gauge(container, size, params={}) {

  // Configure params.
  this.container = container;
  this.size = size;
  this.padding = params.padding || 0;
  this.size = size - this.padding * 2;
  this.width = this.size;
  this.height = this.size;
  this.scaleWidth = params.scaleWidth || 10;
  this.scaleRadius = (this.size - this.scaleWidth) / 2;
  this.scaleCenter = {x: this.size / 2, y: this.size / 2};
  this.values = params.values || [0, 10];
  this.scaleColors = params.scaleColors || ['white'];
  this.minValue = this.values[0];
  this.maxValue = this.values[this.values.length - 1];
  this.handHalfWidth = (params.handWidth || 10) / 2;
  this.handTipHalfWidth = (params.handTipWidth || this.handHalfWidth / 4) / 2;
  this.handColor = params.handColor || 'red';
  this.startAngle = rad(params.startAngle || 0);
  this.sweepAngle = rad(params.sweepAngle || 360);
  this.startPaddingAngle = rad(params.startPaddingAngle || 0);
  this.endPaddingAngle = rad(params.endPaddingAngle || 0);
  this.scaleGap = params.scaleGap || 0;

  // Create the spring.
  var curValue = params.curValue || this.minValue;
  var tension = params.tension || 40;
  var friction = params.friction || 7;
  var overshootClamping = params.overshootClamping || false;

  this.springSystem = new rebound.SpringSystem();
  this.spring = this.springSystem.createSpring(tension, friction);
  this.spring.setOvershootClampingEnabled(overshootClamping);
  this.spring.setCurrentValue(curValue);
  this.spring.addListener(this);

  // Compute scale angles.
  // The first and last are special (they match the start/end exactly, so the scale start/end
  // offsets are included in them). The middle ones are computed.
  this.scaleAngles = [this.startAngle];
  for(var i = 1; i < this.values.length - 1; i++) {
      this.scaleAngles.push(this._angleOf(this.values[i]));
  }
  this.scaleAngles.push(this.startAngle + this.sweepAngle);

  // Create the canvas.
  this.canvas = document.createElement('canvas');
  this.canvas.width = size; // Outer size (with padding).
  this.canvas.height = size; // Outer size (with padding).
  this.ctx = this.canvas.getContext('2d');

  // Do an initial render and replace container contents.
  this.render();
  this.container.innerHTML = '';
  this.container.appendChild(this.canvas);

}

Gauge.prototype = {
  constructor: Gauge,
  render: function() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.save();
      this.ctx.translate(this.padding, this.padding);
      this._drawScale();
      this._drawHand();
    this.ctx.restore();
  },
  _drawScale: function() {
    // Draw the scale
    this.ctx.lineWidth = this.scaleWidth;
    for (var i = 0; i < this.scaleAngles.length - 1; i++) {
      this.ctx.beginPath();
      this.ctx.lineCap = "butt";
      this.ctx.arc(this.scaleCenter.x, this.scaleCenter.y, this.scaleRadius, this.scaleAngles[i], this.scaleAngles[i+1]);
      this.ctx.strokeStyle = this.scaleColors[i];
      this.ctx.stroke();
    }
    // Clear the gaps
    if (this.scaleGap > 0) {
      for (var i = 1; i < this.scaleAngles.length - 1; i++) {
        this.ctx.save();
          this.ctx.translate(this.scaleCenter.x, this.scaleCenter.y);
          this.ctx.rotate(this.scaleAngles[i]);
          this.ctx.clearRect(0, -this.scaleGap/2, this.width/2, this.scaleGap/2)
        this.ctx.restore();
      }
    }
  },
  _drawHand: function() {
    // Draw the hand
    this.ctx.save();
      this.ctx.translate(this.scaleCenter.x, this.scaleCenter.y);
      this.ctx.rotate(this._angleOf(this.spring.getCurrentValue()));
      this.ctx.beginPath();
      this.ctx.moveTo(this.scaleRadius, this.handTipHalfWidth);
      this.ctx.lineTo(0, this.handHalfWidth);
      this.ctx.arc(0, 0, this.handHalfWidth, 0.5*Math.PI, 1.5*Math.PI);
      this.ctx.lineTo(this.scaleRadius, -this.handTipHalfWidth);
      this.ctx.closePath();
      this.ctx.lineJoin = "miter";
      this.ctx.fillStyle = this.handColor;
      this.ctx.fill();
    this.ctx.restore();
  },
  _angleOf: function(value) {
    return this.startAngle + this.startPaddingAngle + this._sweepOf(value - this.minValue);
  },
  _sweepOf: function(delta) {
    return (this.sweepAngle - this.startPaddingAngle - this.endPaddingAngle) * delta / (this.maxValue - this.minValue);
  },
  _clampValue: function(value) {
    return Math.min(Math.max(value, this.minValue), this.maxValue);
  },
  onSpringUpdate: function(spring) {
    this.render();
  },
  setValue: function(value) {
    this.spring.setEndValue(this._clampValue(value));
  },
  getValue: function() {
    return this.pring.getEndValue();
  }
}

module.exports = Gauge;
