/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

'use strict';

var $ = require('jquery');
var webrtc = require('./webrtc');
var rpc = require('./jsonrpc');
var timedRpc = require('./timedjsonrpc');
var Session = require('./session');
var fullscreen = require('jquery-fullscreen-plugin');
var Gauge = require('./gauge');

var headlightsOn = false;

var sessionStatus = function (data) {
  $("p#status").text(data);
}

var initUi = function() {
  $("#progress").hide();
  $("#errorMsg").hide();
  updateHeadlightsButton();
  updateFullScreenButton();
}

var updateHeadlightsButton = function() {
  var icon = headlightsOn ? "#ic_headlights_24px" : "#ic_headlights_off_24px";
  $("#headlights svg use").attr('xlink:href', icon);
}

var updateFullScreenButton = function() {
  var icon = $(document).fullScreen() ? "#ic_fullscreen_exit_24px" : "#ic_fullscreen_24px";
  $("#fullscreen svg use").attr('xlink:href', icon);
}

var progress = function(on) {
  if(on) {
    $("#progress").show();
    $("#progress").addClass("active");
    $("#errorMsg").hide();
  } else {
    $("#progress").hide();
    $("#progress").removeClass("active");
  }
}

var error = function(msg) {
  progress(false);
  $("#errorMsg").show();
  Materialize.toast(msg, 4000)
}

$(document).ready(function () {
  initUi();

  var batteryGauge;
  var rpcGauge;
  var sysinfo = function (data) {
    if (data != null) {
      batteryGauge.setValue(data.batteryVoltage);
      rpcGauge.setValue(-timedRpc.getRpcMillis());
    }
  }


  var joyPos = {distance: 0, angle: 0};
  rpc.setup({
    endPoint: 'http://192.168.0.11:8888',
    cache: false
  });
  timedRpc.attach(rpc);
  var session = new Session(rpc, sessionStatus, sysinfo);
  $(window).unload(function () {
    session.close();
    webrtc.stop();
    return null;
  });
  // Automatically connect on load.
  session.connect();
  webrtc.start({
    error: error,
    progress: progress
  });
  // Camera overlay eats the pointer down events, so they don't get to the joystick.
  $('.pointerReactor').bind('touchstart mousedown pointerdown MSPointerDown', function (event) {
    event.stopPropagation();
  });
  // Headlights button
  $('#headlights').click(function (event) {
    headlightsOn = !headlightsOn;
    session.headlights(headlightsOn);
    updateHeadlightsButton();
  });
  // Manual reload button
  $('#reload').click(function(event) {
    window.location.reload();
  });
  // Gauges.
  var gaugeStyle = {
    padding: 4,
    handTipWidth: 2,
    handColor: '#cc0000',
    handWidth: 6,
    scaleColors: ['#cc0000', 'white'],
    scaleGap: 4,
    scaleWidth: 8,
    startAngle: 135,
    sweepAngle: 270,
    startPaddingAngle: 5,
    endPaddingAngle:5,
  };
  rpcGauge = new Gauge($('#rpc')[0], 56, $.extend(gaugeStyle, {
    values: [-160, -120, 0] // Small values are 'good' -> negate the scale to put 'good' values at the end of the scale.
  }));
  batteryGauge = new Gauge($('#battery')[0], 56, $.extend(gaugeStyle, {
    values: [6.3, 6.85, 8.5]
  }));
  // Fullscreen button
  $('#fullscreen').click(function(event) {
    $("#content").toggleFullScreen();
  });
  $(document).bind("fullscreenchange", function() {
    updateFullScreenButton();
  });
  //Joystick handling.
  var joystick = require('nipplejs').create({
    zone: document.getElementById('content'),
    size:160,
    color: '#F57F17'
  })
  joystick.on('start end', function (evt, data) {
      joyMove(0, 0);
    })
    .on('move', function (evt, data) {
      joyMove(data.distance, data.angle.degree);
    });
  // Map thumb position to speed and angle values.
  // - Scale range with center deadzone (deadzone..50 -> 0..100)
  // - Keep 10 distance and 15 angle values to cut down on RPC.
  var mapThumbPos = function (distance, angle) {
    var deadzone = 5; // deadzone radius
    var d, a;
    if (distance > deadzone) {
      distance = (distance - deadzone) * 100 / (50 - deadzone) + 0.5;
      d = Math.floor(distance / 10) * 10;
      a = Math.floor(angle / 15) * 15;
    } else {
      d = 0;
      a = 0;
    }
    return {distance: d, angle: a};
  };
  // Send driving command if the joystick has moved enough.
  var joyMove = function (distance, angle) {
    var newPos = mapThumbPos(distance, angle);
    if (newPos.distance != joyPos.distance || newPos.angle != joyPos.angle) {
      joyPos = newPos;
      console.log(newPos)
      session.driveSpeedAngle(joyPos.distance, joyPos.angle);
    }
  };
});
