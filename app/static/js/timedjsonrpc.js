/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

var $ = require('jquery');

var rpcMillis = 0;

// Wrap and time the jsonRpc request.
var timedRequest = function (request) {
  function decorator(method, options) {
    var start = Date.now();
    var newOptions = $.extend({}, options);
    newOptions.success = function(data) {
      rpcMillis = Date.now() - start;
      if(options && options.success) {
        options.success(data);
      }
    };
    newOptions.error = function(data) {
      rpcMillis = Date.now() - start;
      if(options && options.error) {
        options.error(data);
      }
    };
    return request.call(this, method, newOptions);
  };
  return decorator;
}

module.exports.getRpcMillis = function() {
  return rpcMillis;
}
// Replace the request function with the timed one.
module.exports.attach = function(jsonRpc) {
  jsonRpc.request = timedRequest(jsonRpc.request);
  return jsonRpc.request;
}
