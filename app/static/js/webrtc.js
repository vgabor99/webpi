/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

'use strict';

var $ = require('jquery');
var signalling_server_hostname = "192.168.0.11";
var signalling_server_address = signalling_server_hostname + ":8080";

var ws = null;
var pc;
var pcConfig = {
  "iceServers": [
    {"urls": ["stun:stun.l.google.com:19302", "stun:" + signalling_server_hostname + ":3478"]}
  ]
};
var pcOptions = {
  optional: [
    {DtlsSrtpKeyAgreement: true}
  ]
};
var mediaConstraints = {
  optional: [],
  mandatory: {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
  }
};

var error = null;
var progress = null;

var RTCPeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
var RTCSessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
var RTCIceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
var URL = window.URL || window.webkitURL;

function createPeerConnection() {
  try {
    console.log(JSON.stringify(pcConfig));
    pc = new RTCPeerConnection(pcConfig, pcOptions);
    pc.onicecandidate = onIceCandidate;
    pc.onaddstream = onRemoteStreamAdded;
    pc.onremovestream = onRemoteStreamRemoved;
    console.log("peer connection successfully created!");
  } catch (e) {
    console.log("createPeerConnection() failed");
  }
}

function onIceCandidate(event) {
  if (event.candidate) {
    var candidate = {
      sdpMLineIndex: event.candidate.sdpMLineIndex,
      sdpMid: event.candidate.sdpMid,
      candidate: event.candidate.candidate
    };
    var command = {
      command_id: "addicecandidate",
      data: JSON.stringify(candidate)
    };
    ws.send(JSON.stringify(command));
  } else {
    console.log("End of candidates.");
  }
}

function onRemoteStreamAdded(event) {
  console.log("Remote stream added:", URL.createObjectURL(event.stream));
  var remoteVideoElement = document.getElementById('remote-video');
  remoteVideoElement.src = URL.createObjectURL(event.stream);
  remoteVideoElement.play();
}

function onRemoteStreamRemoved(event) {
  var remoteVideoElement = document.getElementById('remote-video');
  remoteVideoElement.src = '';
}

function reportProgress(on) {
  if(progress) {
    progress(on);
  }
}

function reportError(msg) {
  if(error) {
    error(msg);
  }
}

module.exports.start = function start(params) {
  error = params.error;
  progress = params.progress;

  if ("WebSocket" in window) {
    reportProgress(true);
    var protocol = location.protocol === "https:" ? "wss:" : "ws:";
    ws = new WebSocket(protocol + '//' + signalling_server_address + '/stream/webrtc');

    function offer(stream) {
      createPeerConnection();
      if (stream) {
        pc.addStream(stream);
      }
      var command = {
        command_id: "offer",
        options: {
          force_hw_vcodec: false,
          vformat: 60
        }
      };
      ws.send(JSON.stringify(command));
      console.log("offer(), command=" + JSON.stringify(command));
    }

    ws.onopen = function () {
      console.log("onopen()");
      offer();
    };

    ws.onmessage = function (evt) {
      var msg = JSON.parse(evt.data);
      //console.log("message=" + msg);
      console.log("type=" + msg.type);

      switch (msg.type) {
        case "offer":
          pc.setRemoteDescription(new RTCSessionDescription(msg),
            function onRemoteSdpSuccess() {
              console.log('onRemoteSdpSucces()');
              pc.createAnswer(function (sessionDescription) {
                pc.setLocalDescription(sessionDescription);
                var command = {
                  command_id: "answer",
                  data: JSON.stringify(sessionDescription)
                };
                ws.send(JSON.stringify(command));
                console.log(command);

              }, function (error) {
                reportError("Failed to createAnswer: " + error);

              }, mediaConstraints);
            },
            function onRemoteSdpError(event) {
              reportError('Failed to set remote description (unsupported codec on this browser?): ' + event);
              doStop();
            }
          );

          var command = {
            command_id: "geticecandidate"
          };
          console.log(command);
          ws.send(JSON.stringify(command));
          break;

        case "answer":
          break;

        case "message":
          reportError(msg.data);
          break;

        case "geticecandidate":
          var candidates = JSON.parse(msg.data);
          for (var i = 0; candidates && i < candidates.length; i++) {
            var elt = candidates[i];
            var candidate = new RTCIceCandidate({sdpMLineIndex: elt.sdpMLineIndex, candidate: elt.candidate});
            pc.addIceCandidate(candidate,
              function () {
                console.log("IceCandidate added: " + JSON.stringify(candidate));
              },
              function (error) {
                console.log("addIceCandidate error: " + error);
              }
            );
          }
          reportProgress(false);
          break;
      }
    };

    ws.onclose = function (evt) {
      if (pc) {
        pc.close();
        pc = null;
      }
    };

    ws.onerror = function (evt) {
      reportError("An error has occurred!");
      ws.close();
    };

  } else {
    reportError("Sorry, this browser does not support WebSockets.");
  }
}

function doStop() {
  document.getElementById('remote-video').src = '';
  if (pc) {
    pc.close();
    pc = null;
  }
  if (ws) {
    ws.close();
    ws = null;
  }
}

module.exports.stop = function stop() {
  if (ws) {
    ws.onclose = function () {
    }; // disable onclose handler first
    doStop();
  }
};

