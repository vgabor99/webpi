/**
 * webpi
 * (c) Gabor Varga <gabor.varga.99@gmail.com>
 */

'use strict';

function Session(jsonRpc, sessionStatus, sysInfo) {
  this.jsonRpc = jsonRpc;
  this.sessionStatus = sessionStatus;
  this.sysInfo = sysInfo;
  this.sid = null;
  this.keepalive = null;
  this.id = 1; // JsonRPC method id
}

Session.prototype = {
  constructor: Session,
  _error: function (data) {
    if (this.sessionStatus) {
      this.sessionStatus(data.error.message || data.error)
    }
  },
  _sessionStatus: function (data) {
    if (this.sessionStatus) {
      this.sessionStatus(data);
    }
  },
  _sysInfo: function (data) {
    if (this.sysInfo) {
      this.sysInfo(data);
    }
  },
  close: function () {
    if (this.sid != null) {
      window.clearInterval(this.keepalive);
      this.keepalive = null;
      this.jsonRpc.request('close', {
          id: this.id++,
          params: {
            sid: this.sid
          }
        }
      );
      this.sid = null;
      this._sessionStatus("Disconnected");
      console.log('closed');
    }
  },
  connect: function () {
    this.jsonRpc.request('connect', {
      id: this.id++,
      params: {
        when: Date.now()
      },
      success: $.proxy(function (data) {
        this.sid = data.result;
        this.keepalive = window.setInterval($.proxy(this.ping, this), 500);
        this._sessionStatus("Connected");
        console.log('connected ' + this.sid);
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
      }, this)
    });
  },
  driveMotorSteer: function (motor, steer) {
    this.jsonRpc.request('driveMotorSteer', {
      id: this.id++,
      params: {
        when: Date.now(),
        sid: this.sid,
        motor: motor,
        steer: steer
      },
      success: $.proxy(function (data) {
        this._sessionStatus("Connected");
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
      }, this)
    });
  },
  driveSpeedAngle: function (speed, angle) {
    this.jsonRpc.request('driveSpeedAngle', {
      id: this.id++,
      params: {
        when: Date.now(),
        sid: this.sid,
        speed: speed,
        angle: angle
      },
      success: $.proxy(function (data) {
        this._sessionStatus("Connected");
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
      }, this)
    });
  },
  driveTracks: function (left, right) {
    this.jsonRpc.request('drive', {
      id: this.id++,
      params: {
        when: Date.now(),
        sid: this.sid,
        left: left,
        right: right
      },
      success: $.proxy(function (data) {
        this._sessionStatus("Connected");
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
      }, this)
    });
  },
  headlights: function (on) {
    this.jsonRpc.request('headlights', {
      id: this.id++,
      params: {
        when: Date.now(),
        sid: this.sid,
        on: on
      },
      success: $.proxy(function (data) {
        this._sessionStatus("Connected");
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
      }, this)
    });
  },
  ping: function () {
    this.jsonRpc.request('ping', {
      id: this.id++,
      params: {
        when: Date.now(),
        sid: this.sid
      },
      success: $.proxy(function (data) {
        this._sessionStatus("Connected");
        this._sysInfo(JSON.parse(data.result));
      }, this),
      error: $.proxy(function (data) {
        this._error(data);
        this._sysInfo(null);
      }, this)
    });
  }
}

module.exports = Session;
