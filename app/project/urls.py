"""
webpi
(c) Gabor Varga <gabor.varga.99@gmail.com>

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic.base import TemplateView

urlpatterns = patterns('',
                       url(r'^robots\.txt$',
                           TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^download$', 'project.views.download', name='download'),
                       url(r'^unsupported$', 'project.views.unsupported', name='unsupported'),
                       url(r'^drive$', 'project.views.drive', name='drive'),
                       url(r'^guestbook/', include('guestbook.urls'))
                       )

# Serve media files in debug
if settings.DEBUG:
    urlpatterns.append(
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))

# Catch-all
urlpatterns.append(url(r'^$', 'project.views.index', name='index'))
