"""
webpi
(c) Gabor Varga <gabor.varga.99@gmail.com>

WSGI config for project project.

It exposes the WSGI callable as a module-level variable named ``application``.

This software is released under the MIT License:
http://www.opensource.org/licenses/mit-license.php
"""

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project.settings.prod")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
