"""
webpi
(c) Gabor Varga <gabor.varga.99@gmail.com>

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from django.shortcuts import render
from django import http
from django.core.urlresolvers import reverse

from django.conf import settings


def index(request):
    return render(request, 'index.html')


def download(request):
    if request.is_android:
        return http.HttpResponseRedirect('https://play.google.com/store/apps/details?id=com.vargag99.lola')
    else:
        return http.HttpResponseRedirect(reverse('unsupported'))


def unsupported(request):
    return render(request, 'unsupported.html')


def drive(request):
    return render(request, 'drive.html')


def error404(request):
    return render(request, '404.html')
