## Webpi (discontinued)

Webpi is the web app to drive a Raspberry Pi powered toy tank from First Person Perspective. (See the server and the Android app [here](https://bitbucket.org/vgabor99/lola).)

![](app/static/img/img_6456.jpg)

## Motivation

Moving out of my confort zone of Android development, for some Python / JavaScript / Web development.

## Technical details

Webpi uses WebRTC for video link and JSON-RPC over HTTP for the control link.

## Discontinuation

The project has served its purpose, I am no longer interested maintaining it.
